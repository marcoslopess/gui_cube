import React from 'react'
import { Switch, Route, Redirect } from 'react-router'


//COMPONENTS
import Home from '../components/home/Home.component'
import UserCrud from '../components/user/UserCrud.component'

export default props =>
    <Switch>
        <Route exact={true} patch="/" component={Home} />
        <Route path="/users" component={UserCrud} />

    </Switch>

