import React from 'react';
import { withRouter } from 'react-router-dom';
import { Switch, Route, Redirect } from 'react-router';

// ===> PAGES / COMPONENTS
import Compras from '../components/compras/dashboard/dashboard.component'
import Global from '../components/compras/necessidade-global/necessidade-global.component'
import Caixas from '../components/compras/caixas/caixas.component'
import Frascos from '../components/compras/frascos-tampas/frascos-tampas.component'
import MateriaPrima from '../components/compras/materia-prima/materia-prima.component'
import Rotulos from '../components/compras/rotulos/rotulos.component'
import ComprasRealizadas from '../components/compras/compras-realizadas/compras-realizadas.component'


class RoutesMain extends React.Component {
    render() {
        return (
            <Switch history={this.props.history}>
                <Route exact path='/' component={Compras} />
                <Route exact path='/dashboard' component={Compras} />
                <Route exact path='/global' component={Global} />
                <Route exact path='/caixas' component={Caixas} />
                <Route exact path='/frascos_tampas' component={Frascos} />
                <Route exact path='/materia_prima' component={MateriaPrima} />
                <Route exact path='/rotulos' component={Rotulos} />
                <Route exact path='/compras_realizadas' component={ComprasRealizadas} />
                <Redirect to="/" component={Compras} />
            </Switch>
            )
        }
    };

export default withRouter(RoutesMain);