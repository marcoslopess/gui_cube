import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import './App.scss'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'


import Logo from '../components/logo/logo.component'
import Nav from '../components/navbar/navbar.component'
import Routes from '../routes/routes.component'
import Footer from '../components/footer/footer.component'


export default props =>
    <BrowserRouter>
        <div className="app">
            <div className="row mr-auto">
                <div>
                    <Logo />
                    <Nav />
                    <Footer />
                </div>
                <div className="col-9">
                    <Routes />
                </div>
            </div>
        </div>
    </BrowserRouter>
