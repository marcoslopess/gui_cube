import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://economia.awesomeapi.com.br'
});



export default instance;