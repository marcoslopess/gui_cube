import React from 'react';
import { connect } from 'react-redux';
import { DashboardLoadingCount } from '../../../redux/sys-inter/sys-inter.actions';

import LoadingDataIcon from '../../../utils/loading-data-icon/loading-data-icon.component';

import { wait } from '../../../utils/multi-tools.js';

import { RequestDefault } from '../../../data_request_components/request-default-mod01.component';

import 'antd/dist/antd.css';
import 'antd-mobile/dist/antd-mobile.css';
import '../AppComponents.css';


class cards_view1 extends React.Component {

   constructor(props) {
	  	super(props);
		this.state = {

			dash_mainCARD: [
				{
				META: [],
				PERC_META: [],
				VENDAS_FAT: [],
				VENDAS_NFAT: [],
				QT_FAT: [],
				QT_NFAT: [],
				QT_TOTAL: [],
				},
			],
		}
   };

   async componentDidMount() {
	  	let data = await RequestDefault('/dashboard_cards_vendasMAIN/')
		do { await wait(333, 'cards_view1')} while (Object.keys(data).length <= 0);
		if (Object.keys(data).length > 0) {
			this.setState({
				dash_mainCARD: data,
				isLoaded: true,
			});
		};
   };

   render() {

	var { isLoaded } = this.state;

	if (!isLoaded) {
		return <LoadingDataIcon/>
	} else {

		// Currency Formatting Function ------------------
		// eslint-disable-next-line
		Number.prototype.formatMAIN = function(c, d, t){
			var n = this,
			// eslint-disable-next-line
			c = isNaN(c = Math.abs(c)) ? 2 : c,
			// eslint-disable-next-line
			d = d === undefined ? "." : d,
			// eslint-disable-next-line
			t = t === undefined ? "," : t,
			// eslint-disable-next-line
			s = n < 0 ? "-" : "",
			// eslint-disable-next-line
			i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
			// eslint-disable-next-line
			j = (j = i.length) > 3 ? j % 3 : 0;
			return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		};

		let cardDATA = [
			{
				META: (this.state.dash_mainCARD['META']).formatMAIN(2,',','.'),
				PERC_META: (this.state.dash_mainCARD["PERC_META"]).formatMAIN(2,',','.'),
				VENDAS_FAT: (this.state.dash_mainCARD["VENDAS_FAT"]).formatMAIN(2,',','.'),
				VENDAS_NFAT: (this.state.dash_mainCARD["VENDAS_NFAT"]).formatMAIN(2,',','.'),
				TOTAL_VENDA: ((this.state.dash_mainCARD["VENDAS_NFAT"]) + (this.state.dash_mainCARD["VENDAS_FAT"])).formatMAIN(2,',','.'),
				QT_FAT: (this.state.dash_mainCARD['QT_FAT']),
				QT_NFAT: (this.state.dash_mainCARD['QT_NFAT']),
				QT_TOTAL: (this.state.dash_mainCARD['QT_TOTAL']),
			},
		];

		this.props.DashboardLoadingCount(1)

		return (
				<div className = 'colITEM'>
					<div className = "col1">
						<h4>
							{"META MENSAL"}
						</h4>

						<h2>
							{'R$ ' + cardDATA[0]["META"]}
						</h2>
					</div>

					<div className = "col2">
						<h4>
							{"TOTAL DE VENDAS"}
						</h4>

						<h2>
							{'R$ ' + cardDATA[0]["TOTAL_VENDA"]}
							{" [ "}
							{cardDATA[0]["QT_TOTAL"]}

							{" ] "}
						</h2>
					</div>

					<div className = "col3">
						<h4>
							{"FATURADOS"}
						</h4>

						<h2>
							{'R$ ' + cardDATA[0]["VENDAS_FAT"]}
							{" [ "}
							{cardDATA[0]["QT_FAT"]}
							{" ] "}
						</h2>
					</div>

					<div className = "col4">
						<h4>
							{"NÂO FATURADOS"}
						</h4>

						<h2>
							{'R$ ' + cardDATA[0]["VENDAS_NFAT"]}
							{" [ "}
							{cardDATA[0]["QT_NFAT"]}
							{" ] "}
						</h2>
					</div>
				</div>
			)
	  	};
  	}
};

const mapDispatchToProps = dispatch => ({
	DashboardLoadingCount: data => dispatch(DashboardLoadingCount(data))
});

export default connect(
	null,
	mapDispatchToProps
)(cards_view1);
