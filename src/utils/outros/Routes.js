import React from 'react';

import { withRouter } from 'react-router-dom';

import { Switch, Route, Redirect } from 'react-router';

// ===> PAGES / COMPONENTS
import CUBE from '../pages/main-page/main-page.component';


class RoutesMain extends React.Component {

	render() {
		return (
			<Switch history={this.props.history}>	
				<Route exact path='/' render={() => <Redirect to='/system/Prometheus_CUBE/mod01/dashboard' />} />	
				<Route exact path='/system/Prometheus_CUBE/mod01/dashboard' component={ CUBE }  />
			</Switch>
		)
	}
};

export default withRouter(RoutesMain);
