//import { SignOut } from '../firebase/firebase.utils';
import { createCookie } from '../cookies_request/cookies-manager'


export function redirectToWebApp(url) {
	return redirectURL(GetUrlDefaultRequest(url)) 
};


// export function signOutFullData(method=0) {
// 	eraseCookies()
// 	SignOut()
// 	if (method >= 1) { redirectURL(GetUrlDefaultRequest(window.location.href, method)) }
// };


export function eraseCookies(cookies) {
	cookies = document.cookie.split(";");
	for (let i = 0; i < cookies.length; i++) {
		let CookieName = cookies[i].split("=")[0];
		createCookie(CookieName, "", -1);
	};
};


export function wait(ms, functionName='', log=0) {
	if (log === 1) { console.log(`\n ... time loop ... ${functionName} \n`) }
	return new Promise(r => setTimeout(r, ms))
};


export function redirectURL(urlDefault, log=0) {
	if (log === 1) { console.log(`\n redirecting ... to: , ${urlDefault} \n`) }
	window.location.replace(urlDefault);
};


export function LocalPortRedirector(localPort, method=0, LocalPortRed='') {

	if (method === 0) {
		switch(localPort) {
			case '4002': return LocalPortRed = ':3002'
			case '4003': return LocalPortRed = ':3003'
			case '3002': return LocalPortRed = ':4002'
			case '3003': return LocalPortRed = ':4003'
			case '8080': return LocalPortRed = ':8080'
			default: LocalPortRed = '';
		}
	} else if (method === 1) { 
		LocalPortRed = ':8080' 
	} else if (method === 2) { 
		LocalPortRed = `:${localPort}` 
	}

	return LocalPortRed
};


export function getUrlRedirect(urlLocal=window.location.href, localPort) {
	let urlWin  	= splitMulti(urlLocal,[':', '/']);
	/* splitMulti ---> return -> [ "http", "", "", "192.168.0.205", "4002", "web_app", "" ] */

	let urlRdr		= `${urlWin[0]}://${urlWin[1]}${urlWin[3]}`;
	localPort 		= LocalPortRedirector(urlWin[4]);
	switch(urlWin[3]) {
		case '190.2.72.123': 	
			urlRdr += (`${localPort}`)
			break;

		case '192.168.0.205': 	
			urlRdr += (`${localPort}`)
			break;

		case '192.168.0.223': 	
			urlRdr += (`${localPort}`)
			break;

		default: 
			urlRdr = 'https://www.prometheuscube.com';
	}

	return urlRdr
};


export function GetUrlDefaultRequest(urlLocal, method=0, localPort, urlRdr='') {
	let urlWin = splitMulti(urlLocal,[':', '/']);
	/* splitMulti ---> return -> [ "http", "", "", "192.168.0.205", "4002", "web_app", "" ] */


	if (method === 1) {
		
		if (urlWin[3] === '190.2.72.123') { 
			urlRdr = 'http://190.2.72.123:8080';
		} else { 
			
			urlRdr = 'https://www.prometheuscube.com'; 
		};

	} else {

		urlRdr = `${urlWin[0]}://${urlWin[1]}${urlWin[3]}`;
		localPort  =  LocalPortRedirector(urlWin[4], method)
		
		switch(urlWin[3]) {
			case '190.2.72.123': 	
				urlRdr += (`${localPort}`)
				break;

			case '192.168.0.205': 	
				urlRdr += (`${localPort}`)
				break;

			case '192.168.0.223': 	
				urlRdr += (`${localPort}`)
				break;

			default: 
				urlRdr = 'https://www.prometheuscube.com';
				
		}
	};

	return urlRdr
};


export async function UserNotFound(urlRdr) {
	urlRdr = GetUrlDefaultRequest()
	redirectURL(urlRdr)
};


//usage: splitMulti('13.887.958/0001-98', ['@', '-', '.', '/']) ---> "13887958000198"
export function splitMulti(str='', tokens){
	let tempChar = tokens[0];
	for(let i = 1; i < tokens.length; i++){
		str = str.split(tokens[i]).join(tempChar);
	}
	str = str.split(tempChar);

	return str;
};


//usage: splitJoinMulti('13.887.958/0001-98', ['@', '-', '.', '/']) ---> Array(5) [ "13", "887", "958", "0001", "98" ]
export function splitJoinMulti(str='', tokens, tmp=''){
	let tempChar = tokens[0];
	for(let i = 1; i < tokens.length; i++){
		str = str.split(tokens[i]).join(tempChar);
	}
	str = str.split(tempChar);
	
	for (let i2 in str) {
		tmp+=str[i2] 
	}

	return tmp;
};


//usage: replaceMulti(['teste@@@', 'tes@.te@', 'tes-@te@@'], ['@', '-', '.', '/']) ---> Array(3) [ "teste", "teste", "teste" ]
export function replaceMulti(str='', tokens){
	for(let i = 0; i < tokens.length; i++){
		for (let j in str) {
			let strTmp = ''
			for (let x in str[j]) {
				strTmp += (str[j][x].replace(tokens[i],""))
				console.log('str[j][x]: ', str[j][x], x)
			}
			str[j] = strTmp
		}
	}

	return str;
};
