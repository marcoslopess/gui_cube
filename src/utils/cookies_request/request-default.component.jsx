import { wait, GetUrlDefaultRequest, getUrlRedirect } from './multi-tools';

import { DictDefaultOptions } from './crypto-data';

import cookies_data from '../json/autenticacao/autenticacao.json'

import { createCookie, readCookie } from './cookies-manager'



export async function RequestDefault(requestURL, authLevel=0, extraArgs='') {

	for(let cookie in cookies_data){
		if (cookie === 'f') {
			createCookie(cookie, JSON.stringify(cookies_data[cookie]))
		} else {	
			createCookie(cookie, cookies_data[cookie])
		}
	}

	let urlRdr			= GetUrlDefaultRequest(getUrlRedirect(),1)  
	let cookiesreader1 	= `u=${readCookie('u')}&g=${readCookie('g')}&i=${readCookie('i')}`;
	let cookiesreader2 	= `&l=${readCookie('l')}&e=${readCookie('e')}&p=${readCookie('p')}`; 
	let url 			= (`${urlRdr}${requestURL}?${cookiesreader1}${cookiesreader2}`);
	
	if (extraArgs !== '') { url += extraArgs };

	let defaultOptions 	= DictDefaultOptions(url,'GET');
	let sentData 		= { method:defaultOptions.method, mode: defaultOptions.mode };
	let data 			= [];

	console.log(url);
	
	await fetch(url, sentData)
	.then(response => response.json())
	.then(responseText => { data = (JSON.parse(JSON.stringify(responseText))) })
	.catch(error => { console.log(`${requestURL} error: `, error) });
	
	do { await wait(333, requestURL) } while (Object.keys(data).length <= 0);
	if (Object.keys(data).length >= 1) { 
		return {
			data
		} 
	 };

};

