import { GetUrlDefaultRequest } from './multi-tools';

export const config = {
	appId				: "1:689373502205:web:8abc3157b20a6c2e357c94"		,
	apiKey				: "AIzaSyD82lYrgcv12jvNXwVG0B966heSSVxi5nA"			,
	projectId			: "prometheus-server-api-01"						,
	authDomain			: "prometheus-server-api-01.firebaseapp.com"		,
	databaseURL			: "https://prometheus-server-api-01.firebaseio.com"	,
	measurementId		: "G-TM46TH59GN"									,
	storageBucket		: "prometheus-server-api-01.appspot.com"			,
	messagingSenderId	: "689373502205"
};

export const DictDefaultOptions = (url, method='GET') => {
	let defaultOptions = {
		url     : url	                               ,
		method  : method                               ,
		mode    : 'cors'                               ,
		headers : {'Access-Control-Allow-Origin':'*'}  ,
	};

	return defaultOptions
};


export async function GetPublicIp(userIP={}) {
	let url 			= (`${await GetUrlDefaultRequest(window.location.href)}/get_client_ip/`);
	let defaultOptions 	= DictDefaultOptions(url,'GET');
	let sentData 		= { method:defaultOptions.method, mode: defaultOptions.mode };

	fetch(defaultOptions.url, sentData)
		.then(response => response.json())
		.then(responseText => { userIP = JSON.parse(JSON.stringify(responseText)) })
		.catch(error => { errorStr(error) })

		do { await wait(333, 'GetPublicIp') } while (Object.keys(userIP).length <= 0);
		if (Object.keys(userIP).length > 0) { return userIP.ip_address }
};


export function wait(ms, functionName='', log=0) {
	if (log === 1) { console.log(`\n ... time loop ... ${functionName} \n`) }
	return new Promise(r => setTimeout(r, ms))
};


export function redirectURL(urlDefault='https://www.prometheuscube.com', log=0) {
	if (log === 1) { console.log(`\n redirecting ... to: , ${urlDefault} \n`) }
	window.location.replace(urlDefault);
};


export function errorStr(error) {
	document.querySelector('#auth-errorcode').innerHTML = error.message

	let authInputs = document.querySelectorAll('#authInputs');
	for (let a = 0; a < authInputs.length; a++) { authInputs[a].style.display = 'flex' }

	let loader = document.querySelectorAll('#loading_wait');
	for (let b = 0; b < loader.length; b++) { loader[b].style.display = 'none' }
};


export async function shuffleDataSend(word, urlPREFIX, em, hashSource, hashAux='') {
	let url 			= (`${urlPREFIX}/e_e_e_e_e_e_hashAux/?a=${em}&t=${hashSource}`);
	let defaultOptions 	= DictDefaultOptions(url,'GET');
	let sentData 		= { method:defaultOptions.method, mode: defaultOptions.mode };

	fetch(url, sentData)
		.then(response => response.json())
		.then(responseText => { hashAux = responseText.hashAux })
		.catch(error => { errorStr(error) });
	
	do { await wait(333, 'shuffleDataSend') } while (Object.keys(hashAux).length <= 0);
	
	if (Object.keys(hashAux).length > 0) {
		word += hashAux;
		let shuffledWord = '';
		word = word.split('');
		while (word.length > 0) { shuffledWord +=  word.splice(word.length * Math.random() << 0, 1) }
		return shuffledWord;
	};
};


export function verifyEnv(PublicIP, env) {
	switch(PublicIP.ip_address) {
		case '190.2.72.123': 	
			return 'http://190.2.72.123:8080';

		case '192.168.0.205': 	
			return 'http://192.168.0.205:8080';

		case '192.168.0.223': 	
			return 'http://192.168.0.205:8080';

		default: 
			return 'https://www.prometheuscube.com';
	}
};


export async function EcryP_rt(	em						,
								ps						,
								uuid					,
								methodAuth				,
								idtoken					,
								AuthData				,
								methodRedirect	= 0		,
								data			= []	,
								PublicIP		= ''	,
								AuthReq			= ''	,
								salt			= ''	,
								env				= ''	,
								url				= ''		) {

	url = (`${await GetUrlDefaultRequest(window.location.href,1)}/get_client_ip/`);

	let defaultOptions 	= DictDefaultOptions(url,'GET');
	let sentData 		= { method:defaultOptions.method, mode:defaultOptions.mode };

	fetch(url, sentData)
		.then(response => response.json())
		.then(responseText => { PublicIP = JSON.parse(JSON.stringify(responseText)) })
		.catch(error => { errorStr(error) });

	do { await wait(333, 'EcryP_rt') } while (Object.keys(PublicIP).length <= 0);

	if (Object.keys(PublicIP).length > 0) {
		env 			= verifyEnv(PublicIP)

		ps				= await shuffleDataSend(ps, env, em, 0)
		uuid 			= await shuffleDataSend(uuid, env, em, 1)
		idtoken 		= await shuffleDataSend(idtoken, env, em, 2)

		url 			= `${env}/e_e_e_e_e_e/?a=${ps}&b=${em}&c=${uuid}&d=${idtoken}`;

		defaultOptions 	= DictDefaultOptions(url,'GET');
		sentData 		= { method:defaultOptions.method, mode:defaultOptions.mode };
		AuthReq 		= '/prometheus_GUI_CUBE_Auth_UUID/'

		fetch(url, sentData)
		.then(response => response.json())
		.then(responseText => {
			data.push(JSON.parse(JSON.stringify(responseText)));
			ps  		= (data[0].d)
			em  		= (data[0].b)
			salt 		= (data[0].a)
			uuid		= (data[0].f)
			idtoken 	= (data[0].e)
			AuthData	= JSON.stringify(AuthData)
			
			url 		= `${env}${AuthReq}
								?a=${ps}
								&b=${em}
								&c=${salt}
								&d=${uuid}
								&m=${methodAuth}
								&e=${idtoken}
								&f=${AuthData}`;

			if (methodRedirect === 0) { redirectURL(url) }

			if (methodRedirect === 1) {
				let userData = []
				fetch(url, sentData)
				.then(response => response.json())
				.then(responseText => {   
					userData = JSON.parse(JSON.stringify(responseText)) 
					return userData
				})
				.catch(error => { errorStr(error) });
			} 
		
		})
		.catch(error => { errorStr(error) });
	}
};
