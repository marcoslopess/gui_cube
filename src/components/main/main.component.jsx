import './main.styles.scss'
import React from 'react'
import Header from'../header/header.component'

export default props =>
<React.Fragment>
    <div className="teste">
    <Header {...props} />
    <main className="mr-auto">
        <div className="p-3 mt-3">
            {props.children}
        </div>
    </main>
    </div>
   
</React.Fragment>
