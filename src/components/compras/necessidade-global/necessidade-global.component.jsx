import React from 'react'
import './necessidade-global.styles.scss'
import Main from '../../main/main.component'

import data from '../../../utils/json/compras-global/necessidade-compras.json'

export default props =>
  <Main icon="globe" title="Necessidade Global"
    subtitle="Todos os itens que precisam ser comprados!">
    <div className="table-responsive">
      <table className="table" >
        <thead>
            <tr >
            {data.map((dados, index) => (
            <th key={index} scope="col">{Object.keys(dados)[index]}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((dados, index) => (
            <tr key={index}>
              <td>{dados.DESCRICAO}</td>
              <td>{dados.SECAO}</td>
              <td>{dados.TIPO_MERC}</td>
              <td>{dados.QTD_DISPON}</td>
              <td>{dados.QTD_NECESSIDADE}</td>
              <td>{dados.QTD_PEDIDA}</td>
              <td></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  </Main>