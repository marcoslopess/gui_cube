import React from 'react'
import './rotulos.styles.scss'
import Main from '../../main/main.component'

import data from '../../../utils/json/compras-global/necessidade-compras.json'

export default props =>
  <Main icon="sticky-note" title="Rótulos"
    subtitle="Todos os rótulos que precisam ser comprados!">
    <div className="table-responsive">
      <table className="table" >
        <thead>
          <tr>
            <th scope="col">CODPROD</th>
            <th scope="col">DESCRIÇÃO</th>
            <th scope="col">SEÇÃO</th>
            <th scope="col">TIPO MERC</th>
            <th scope="col">DISPONIVEL</th>
            <th scope="col">NECESSIDADE</th>
            <th scope="col">PEDIDA</th>
            <th scope="col">QTD NECESSARIA</th>
          </tr>
        </thead>
        <tbody>
          {data.map((dados, index) => (
            <tr key={index}>
              <td>{dados.CODPROD}</td>
              <td>{dados.DESCRICAO}</td>
              <td>{dados.SECAO}</td>
              <td>{dados.TIPO_MERC}</td>
              <td>{dados.QTD_DISPON}</td>
              <td>{dados.QTD_NECESSIDADE}</td>
              <td>{dados.QTD_PEDIDA}</td>
              <td></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  </Main>