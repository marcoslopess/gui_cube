import React from 'react';

import './dollar.styles.scss'

import { wait } from '../../../utils/cookies_request/multi-tools'

import { RequestDefault } from '../../../utils/cookies_request/request-default.component'
class cotacao_dollar extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			dollar: {}
		}
	};

	async componentDidMount() {

		let data = await RequestDefault('/hypercubo_compras/get_dollar_values/')
		do { await wait(333, 'cotacao_dolar') } while (Object.keys(data).length <= 0);
		if (Object.keys(data).length > 0) {
			this.setState({
				dollar: data
			});
		};
	};	

	render() {

		const Array = Object.values(this.state.dollar)
		
		let dateFormated
		

		return (
			<div className="col align-self-end card-footer">
				<div className="card border-success card-box " >
					{Array.map((index, values) => (
						<div className="card-body text-success" key={values}>
							<p className="nome-titulo">DÓLAR COMERCIAL</p>
							<p className="card-text valor">COMPRA: R$ {index.PRECO_COMPRA}</p>
							<p className="card-text valor">VENDA: R$ {index.PRECO_VENDA}</p>
							<p className=" ultima-atualizacao">Última atualização:
								{console.log(dateFormated = new Date(index.DATA_ATUALIZACAO))}
								<br />
								dia {dateFormated.toLocaleDateString()}
								<br />
								ás {dateFormated.toLocaleTimeString()}
							</p>
						</div>
					))}
					<button type="button" className="btn btn-success texto">
					 	<a className="texto" href="/"> Atualizar </a>
					</button>
				</div>
			</div>
		)
	};
};

export default (cotacao_dollar);
