import './navbar.styles.scss'
import NavbarItem from './navbar-item/navbar-item.component'
import React from 'react'
import Dolar from './dollar/dollar.component'


export default props =>
<aside className="menu-area">
<nav className="nav flex-column menu">
    <NavbarItem url="dashboard" icon="home" title="Início" />
    <NavbarItem url="global" icon="globe" title="Necessidade Global" />
    <NavbarItem url="caixas" icon="archive" title="Caixas" />
    <NavbarItem url="frascos_tampas" icon="bitbucket" title="Frascos e Tampas" />
    <NavbarItem url="materia_prima" icon="flask" title="Materia Prima" />
    <NavbarItem url="rotulos" icon="sticky-note" title="Rótulos" />
    <NavbarItem url="compras_realizadas" icon="check-circle" title="Compras Realizadas" />
</nav>
<Dolar/>
</aside>
