import './logo.styles.scss'
//logo antiga
//import logo from '../../assets/imgs/cube.png'
//logo nova com svg
import { ErpCloud } from '../../assets/svg_products/logo/logo.component'
import React from 'react'

export default props =>
        <aside className="logo">
                {/* <a href="#/" className="logo">
                                <img src={logo} alt="logo" />
</a> */}
                <ErpCloud className="main_logo_brand_erp_cloud" x="0" y="0" heigth={'55px'}
                        width={'55px'} />
        </aside>
