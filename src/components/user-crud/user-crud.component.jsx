import  React, { Component } from 'react'
import Main from '../main/main.component'

const headerProps = {
    icon: 'users', 
    title: 'Usuarios',
    subtitle: 'Cadastro de usuarios...'
}

export default class UserCrud extends Component {
    render() {
        return (
            <Main {...headerProps}>
               
               <div> Cadastro de Usuários
                   </div>
            </Main>
        )
    }
}