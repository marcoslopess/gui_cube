import './header.styles.scss'
import React from 'react'

export default props =>
    <header className="header">
        <nav className="navbar">
            <div className="">
                <h1 className="">
                    <i className={`fa fa-${props.icon}`}></i> {props.title}
                </h1>
                <p className="lead text-muted">{props.subtitle}</p>
            </div>
        </nav>
    </header>